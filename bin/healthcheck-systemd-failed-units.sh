#! /usr/bin/env bash

# check for any systemd failed units
#
# TODO: what should we do if this is in "warning"?
# TODO: what should we do if this is in "error"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

# - (NetworkManager|systemd-networkd)-wait-online-service: inconclusive, and network issues are obvious in other ways
# - fwupd-refresh.service: https://bugs.launchpad.net/ubuntu/+source/fwupd/+bug/1969976 and https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=943343
# - netdata-updater.service: not a big deal if netdata's self-update doesn't work
IGNORE_LIST="NetworkManager-wait-online.service fwupd-refresh.service netdata-updater.service systemd-networkd-wait-online.service"
# - healthcheck-report.service: no need for a reporting failure to be counted twice
IGNORE_LIST="${IGNORE_LIST} healthcheck-report.service"

JQ_QUERY="map(.unit)"
for IGNORE in ${IGNORE_LIST}; do
  JQ_QUERY="${JQ_QUERY} | map(select(. != \"${IGNORE}\"))"
done
JQ_QUERY="${JQ_QUERY} | .[] "

FAILED_UNITS=$(systemctl list-units --output=json --state=failed | jq --raw-output "${JQ_QUERY}")
FAILED_OUTPUT=$(systemctl list-units --state=failed)

if [ -z "${FAILED_UNITS}" ]; then
  touch "${REPORTS_DIR}/ok/${NAME}"
else
  # include the ignored units in the report,
  # they just don't count against the alert threshold
  echo "${FAILED_OUTPUT}" >"${REPORTS_DIR}/warning/${NAME}"
fi
