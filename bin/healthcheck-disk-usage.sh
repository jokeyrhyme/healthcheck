#! /usr/bin/env bash

# check for any volumes that are too full
#
# TODO: what should we do if this is in "warning"?
# TODO: what should we do if this is in "error"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

ERROR_PERCENT=85
WARNING_PERCENT=70
# exclude btrfs because we need to use btrfs-progs for that
# exclude devtmpfs and tmpfs because we'll cover CPU and RAM separately
CURRENT_PERCENT=$(df --exclude-type=btrfs --exclude-type=devtmpfs --exclude-type=tmpfs --local --output=pcent | grep --only-matching '[[:digit:]]*' | sort --numeric-sort --reverse | head --lines=1)
if [ "${CURRENT_PERCENT}" -gt "${ERROR_PERCENT}" ]; then
  df --exclude-type=devtmpfs --exclude-type=tmpfs --local >"${REPORTS_DIR}/error/${NAME}"
elif [ "${CURRENT_PERCENT}" -gt "${WARNING_PERCENT}" ]; then
  df --exclude-type=devtmpfs --exclude-type=tmpfs --local >"${REPORTS_DIR}/warning/${NAME}"
else
  touch "${REPORTS_DIR}/ok/${NAME}"
fi
