#! /usr/bin/env bash

# TODO: what does this check?
# TODO: what should we do if this is in "warning"?
# TODO: what should we do if this is in "error"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

for DEVICE in $(mount --types btrfs | cut -d ' ' -f 1 | uniq); do
  if btrfs device stats --check "${DEVICE}"; then
    touch "${REPORTS_DIR}/ok/${NAME}"
  else
    btrfs device stats --check "${DEVICE}" >"${REPORTS_DIR}/error/${NAME}"
  fi
done
