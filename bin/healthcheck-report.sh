#! /usr/bin/env bash

# usage:
#   path/to/healthcheck-report.sh [--json]
#
# options:
#   --json: output JSON, always with exit code 0
#
# variables:
#   HEALTHCHECK_IO_ERROR:   key for posting to https://healthchecks.io
#   HEALTHCHECK_IO_WARNING: key for posting to https://healthchecks.io

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"

__healthcheck_has_reports() { # $1=category
  if [ ! -d "${REPORTS_DIR}/${1}" ]; then
    false
  else
    # find regular files that were created in the day,
    # this means we'll find nothing if reports have gone stale
    [ -n "$(find "${REPORTS_DIR}/${1}" -maxdepth 1 -mtime -1 -type f -print)" ]
  fi
}

__healthcheck_logs() {
  for LOG in find "${REPORTS_DIR}"/{error,warning}/*; do
    echo "${LOG}"
    cat "${LOG}"
    echo ""
  done
}

__healthcheck_ping_healthchecks_io_no_error() {
  if [ -n "${HEALTHCHECK_IO_ERROR-}" ]; then
    # no "error" reports, so ping https://healthchecks.io
    curl --fail --output /dev/null --retry 2 --show-error --silent "https://hc-ping.com/${HEALTHCHECK_IO_ERROR-}"
  fi
}

__healthcheck_ping_healthchecks_io_no_warning() {
  if [ -n "${HEALTHCHECK_IO_WARNING-}" ]; then
    # no "error" or "warning" reports, so ping https://healthchecks.io
    curl --fail --output /dev/null --retry 2 --show-error --silent "https://hc-ping.com/${HEALTHCHECK_IO_WARNING-}"
  fi
}

__healthcheck_send_logs_healthchecks_io_error() {
  if [ -n "${HEALTHCHECK_IO_ERROR-}" ]; then
    # send "error" reports to https://healthchecks.io
    curl --data-raw "$(__healthcheck_logs)" --fail --output /dev/null --request POST --retry 2 --show-error --silent "https://hc-ping.com/${HEALTHCHECK_IO_ERROR-}/fail"
  fi
}

__healthcheck_send_logs_healthchecks_io_warning() {
  if [ -n "${HEALTHCHECK_IO_WARNING-}" ]; then
    # send "warning" reports to https://healthchecks.io
    curl --data-raw "$(__healthcheck_logs)" --fail --output /dev/null --request POST --retry 2 --show-error --silent "https://hc-ping.com/${HEALTHCHECK_IO_WARNING-}/fail"
  fi
}

__healthcheck_json_array() { # ${1} = error|ok|warning
  if [ -d "${REPORTS_DIR}/${1}" ]; then
    CHECKS=''
    for FILE in "${REPORTS_DIR}/${1}"/*; do
      if [ "$(basename "${FILE}")" = "*" ]; then
        continue
      fi
      CHECKS+=,'"'
      CHECKS+="$(basename "${FILE}" | cut -d- -f2- -)"
      CHECKS+='"'
    done
    CHECKS=[$(echo "${CHECKS}" | cut -c2- -)]
    echo "${CHECKS}"
  else
    echo '[]'
  fi
}

__healthcheck_json() {
  ERROR="$(__healthcheck_json_array error)"
  OK="$(__healthcheck_json_array ok)"
  WARNING="$(__healthcheck_json_array warning)"
  jq --compact-output --null-input --argjson error "${ERROR}" --argjson ok "${OK}" --argjson warning "${WARNING}" '{"error":$error,"ok":$ok,"warning":$warning}'
}

if [ "${1-}" = "--json" ]; then
  __healthcheck_json

elif __healthcheck_has_reports error; then
  echo "${REPORTS_DIR}/error:"
  ls "${REPORTS_DIR}/error"
  __healthcheck_send_logs_healthchecks_io_error

elif __healthcheck_has_reports warning; then
  echo "${REPORTS_DIR}/warning:"
  ls "${REPORTS_DIR}/warning"
  __healthcheck_ping_healthchecks_io_no_error
  __healthcheck_send_logs_healthchecks_io_warning

elif __healthcheck_has_reports ok; then
  echo "${REPORTS_DIR}/ok:"
  ls "${REPORTS_DIR}/ok"
  __healthcheck_ping_healthchecks_io_no_error
  __healthcheck_ping_healthchecks_io_no_warning

else
  MESSAGE="no reports found, checks might be broken or delayed"
  echo "healthcheck: ${MESSAGE}"
  exit 1
fi
