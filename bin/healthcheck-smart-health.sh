#! /usr/bin/env bash

# TODO: what does this check?
# TODO: what should we do if this is in "warning"?
# TODO: what should we do if this is in "error"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

# it's a bit tricky to conditionally have this exclusion list or not,
# so here we always have a list of at least one item to make later code simpler
#
# the kernel reserves the `0` major number, it is used to request a dynamic number,
# so this should be okay to always exclude here by default
EXCLUDE_MAJORS=0

# exclude major numbers for MMC devices: smartmontools don't work on them
# TODO: use mmc-utils or other MMC-compatible healthcheck
# exclude major numbers for mem,zram: smartmontools doesn't work on RAM
while IFS= read -r MAJOR; do
  EXCLUDE_MAJORS=${EXCLUDE_MAJORS},$(echo "${MAJOR}" | tr --delete '[:space:]')
done < <(grep '[[:digit:]]\+ \(mem\|mmc\|zram\)' </proc/devices | sed 's/ \(mem\|mmc\|zram\)//')

# grab info about real disk devices, no partitions, etc
DEVICES_INFO=$(lsblk --exclude="${EXCLUDE_MAJORS}" --json --nodeps --output=name,subsystems,type --paths)
NUM_DISKS=$(echo "${DEVICES_INFO}" | jq --raw-output '.blockdevices | length')

if [ "${NUM_DISKS}" -eq 0 ]; then
  # no compatible disks, all done
  touch "${REPORTS_DIR}/ok/${NAME}"
  exit 0
fi

# smartd.service on Archlinux, smartmontools.service on Raspberry Pi OS
for SERVICE in smartd.service smartmontools.service; do
  if systemctl is-failed "${SERVICE}" >/dev/null; then
    systemctl status "${SERVICE}" >"${REPORTS_DIR}/warning/${NAME}"
    exit 0
  fi
done

JQ_QUERY='.blockdevices[].name'
for DEVICE in $(echo "${DEVICES_INFO}" | jq --raw-output "${JQ_QUERY}"); do
  if smartctl --health "${DEVICE}" >/dev/null; then
    touch "${REPORTS_DIR}/ok/${NAME}"
  elif smartctl --device scsi --health "${DEVICE}" >/dev/null; then
    # `--device auto` (default) can fail with "Unknown USB bridge",
    # but succeeds with `--device scsi` for some devices
    touch "${REPORTS_DIR}/ok/${NAME}"
  else
    smartctl --health "${DEVICE}" >"${REPORTS_DIR}/error/${NAME}"
  fi
done
