#! /usr/bin/env bash

# check for available firmware updates
#
# TODO: what should we do if this is in "warning"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

fwupdmgr refresh >/dev/null 2>&1 || true # non-zero exit code when self-throttling

# `fwupdmgr get-updates` either outputs nothing with a zero exit code (no updates),
# or outputs details to stderr with a non-zero exit code (maybe no updates)
if fwupdmgr get-updates >/dev/null 2>&1; then
  touch "${REPORTS_DIR}/ok/${NAME}"
elif (fwupdmgr get-updates 2>&1 || true) | grep --quiet 'No detected devices'; then
  touch "${REPORTS_DIR}/ok/${NAME}"
elif (fwupdmgr get-updates 2>&1 || true) | grep --quiet 'No updatable devices'; then
  touch "${REPORTS_DIR}/ok/${NAME}"
elif (fwupdmgr get-updates 2>&1 || true) | grep --quiet 'No devices can be updated: Nothing to do'; then
  touch "${REPORTS_DIR}/ok/${NAME}"
elif (fwupdmgr get-updates 2>&1 || true) | grep --quiet 'No updates available'; then
  touch "${REPORTS_DIR}/ok/${NAME}"
elif (fwupdmgr get-updates 2>&1 || true) | grep --quiet 'No updates available for remaining devices'; then
  touch "${REPORTS_DIR}/ok/${NAME}"
else
  fwupdmgr get-updates >"${REPORTS_DIR}/warning/${NAME}"
fi
