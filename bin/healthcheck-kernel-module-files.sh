#! /usr/bin/env bash

# check that kernel module files for current kernel exist
#
# TODO: what should we do if this is in "error"?

set -eu -o pipefail

REPORTS_DIR="${REPORTS_DIR:-/run/healthcheck}"
mkdir -p "${REPORTS_DIR}"/{ok,error,warning}
chmod a+rx "${REPORTS_DIR}"/{ok,error,warning}

NAME=$(basename -s .sh "${0}")
rm -f "${REPORTS_DIR}"/{ok,error,warning}/"${NAME}"

VERSION=$(uname --kernel-release)
if [ -d "/usr/lib/modules/${VERSION}" ] || [ -d "/lib/modules/${VERSION}" ]; then
  touch "${REPORTS_DIR}/ok/${NAME}"
else
  echo "old kernel: $(uname --kernel-release)" >"${REPORTS_DIR}/error/${NAME}"
fi
