#! /usr/bin/env bash

setup_suite() {
  if command -v apk; then
    apk update
    apk add jq
  fi
}
