#! /usr/bin/env bats

@test "existing REPORTS_DIR/{error,warning,ok}/* -> exit with status code 0" {
  run mkdir -p "${BATS_TEST_TMPDIR}"/ok
  run touch "${BATS_TEST_TMPDIR}"/ok/healthcheck-foo

  export REPORTS_DIR="${BATS_TEST_TMPDIR}"
  run ./bin/healthcheck-report.sh

  [ "$status" -eq 0 ]
}

@test "missing REPORTS_DIR/{error,warning,ok}/* -> exit with status code 1" {
  export REPORTS_DIR="${BATS_TEST_TMPDIR}"
  run ./bin/healthcheck-report.sh

  [ "$status" -eq 1 ]
  [ "$output" = "healthcheck: no reports found, checks might be broken or delayed" ]
}

@test "stale REPORTS_DIR/ok/* -> exit with status code 1" {
  run mkdir -p "${BATS_TEST_TMPDIR}"/ok
  run touch --date "1999-12-31 23:59:59" "${BATS_TEST_TMPDIR}"/ok/healthcheck-foo

  export REPORTS_DIR="${BATS_TEST_TMPDIR}"
  run ./bin/healthcheck-report.sh

  [ "$status" -eq 1 ]
  [ "$output" = "healthcheck: no reports found, checks might be broken or delayed" ]
}

@test "--json with missing REPORTS_DIR/{error,warning,ok}/* -> output JSON" {
  export REPORTS_DIR="${BATS_TEST_TMPDIR}"
  run ./bin/healthcheck-report.sh --json

  [ "$status" -eq 0 ]
  [ "$output" = '{"error":[],"ok":[],"warning":[]}' ]
}

@test "--json with 3x errors, 1x ok, 1x warning -> output JSON" {
  run mkdir -p "${BATS_TEST_TMPDIR}"/error
  run touch "${BATS_TEST_TMPDIR}"/error/healthcheck-error1
  run touch "${BATS_TEST_TMPDIR}"/error/healthcheck-error2
  run touch "${BATS_TEST_TMPDIR}"/error/healthcheck-error3
  run mkdir -p "${BATS_TEST_TMPDIR}"/ok
  run touch "${BATS_TEST_TMPDIR}"/ok/healthcheck-ok1
  run mkdir -p "${BATS_TEST_TMPDIR}"/warning
  run touch "${BATS_TEST_TMPDIR}"/warning/healthcheck-warning1

  export REPORTS_DIR="${BATS_TEST_TMPDIR}"
  run ./bin/healthcheck-report.sh --json

  [ "$status" -eq 0 ]
  [ "$output" = '{"error":["error1","error2","error3"],"ok":["ok1"],"warning":["warning1"]}' ]
}
