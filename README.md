# healthcheck ![Status](https://img.shields.io/badge/status-actively--developed-brightgreen) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/healthcheck?branch=main)

a simple hobby-level approach to Linux system monitoring

## requirements

- [bash](https://www.gnu.org/software/bash/), [curl](https://curl.se/), [jq](https://github.com/stedolan/jq)

- [smartmontools](https://www.smartmontools.org/)

- [systemd](https://systemd.io/)

- [btrfs-progs](https://github.com/kdave/btrfs-progs) (if you have btrfs volumes)

## goals

- able to display errors/warnings on interactive systems with [eww](https://github.com/elkowar/eww), [waybar](https://github.com/Alexays/Waybar), [yambar](https://gitlab.com/dnkl/yambar), [polybar](https://github.com/polybar/polybar), etc

- able to trigger email or other notifications for headless systems

- decouple error/warning detection from display/notification,
  so that error-detection processes can take varying amounts of time to finish,
  and can run on different schedules,
  without impacting each other or the separately-wired display/notification process

- works with recent installations of [armbian](https://www.armbian.com/) and [archlinux](https://archlinux.org/)

- minimal dependencies

- minimal/zero financial cost

## design

note: for brevity and size reasons,
we have shortened the names of the scripts in this diagram

```
                         +-----+    +------------------+
                         | GUI +----> report.sh --json |
                         +-----+    +----------+-------+
                                               |
+-------+    +--------------+    +-------------v----------+
| timer +----> check_foo.sh +---->                        |
+-------+    +--------------+    |                        |
                                 | /run/healthcheck       |
+-------+    +--------------+    |                        |
| timer +----> check_bar.sh +---->                        |
+-------+    +--------------+    +-----^------------------+
                                       |
   +------------------------+    +-----+-----+    +-------+
   | https://healthcheck.io <----+ report.sh <----+ timer |
   +------------------------+    +-----------+    +-------+
```

- scripts that perform each check (e.g. `check_foo.sh` above),
  write to `/run/healthcheck/{ok,warning,error}/foo`
  (after deleting any of those from previous runs)

- when running on a headless machine,
  configure the script that aggregates the results (e.g. `report.sh` above),
  to ping our selected monitoring platform (https://healthcheck.io)

- when used with an interactive machine / GUI,
  configure it to display the results of running the results script (e.g. `report.sh --json` above)

## assumptions

- you mount /run as a tmpfs volume, i.e. it exists in RAM,
  and writing to it does not cause wear-and-tear on SSDs or mechanical hard drives

- you configure [smartmontools](https://www.smartmontools.org/) perhaps like my [/etc/smartd.conf](https://gitlab.com/jokeyrhyme/dotfiles/-/blob/master/config/smartd.conf)

## installation

- run `make install` in the root of this project with root user permissions

- configure /etc/smartd.conf, like here:

### headless (manual installation)

- go to https://healthchecks.io and register 2x new checks for each headless machine,
  obtaining the UUIDs for use as the HEALTHCHECK_IO_ERROR and HEALTHCHECK_IO_WARNING variables

- run `systemctl edit healthcheck-report.service` with something like:

  ```
  [Service]
  Environment=HEALTHCHECK_IO_ERROR="your UUID here" HEALTHCHECK_IO_WARNING="your other UUID here"
  ```

- run `systemctl enable --now healthcheck-report.timer`

## features / roadmap

- [x] storage device health e.g. SMART status short/long scans

- [x] btrfs device errors

- [ ] btrfs filesystem and multi-device array status

- [ ] btrfs snapshot freshness

- [x] filesystem available space / disk usage

- [ ] filesystem available space / disk usage for btrfs

- [x] kernel module files for current kernel

- [ ] [lynis](https://github.com/CISOfy/lynis)

- [x] systemd units with errors

- [x] available firmware updates via [`fwupdmgr`](https://github.com/fwupd/fwupd)

- [ ] [clamav](https://www.clamav.net/)
