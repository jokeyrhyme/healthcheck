.PHONY: all clean fmt test install uninstall

fmt:
	./scripts/fmt.sh

test:
	./scripts/test.sh

install:
	./scripts/install.sh

uninstall:
	./scripts/uninstall.sh
