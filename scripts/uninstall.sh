#! /usr/bin/env bash

set -eu -o pipefail

for TIMER in ./systemd/*.timer; do
  systemctl disable --now "$(basename "${TIMER}")"
done
for UNIT in ./systemd/*.{service,timer}; do
  rm -fv /etc/systemd/system/"$(basename "${UNIT}")"
done
systemctl daemon-reload

for BIN in ./bin/*.sh; do
  rm -fv /usr/local/bin/"$(basename "${BIN}")"
done
rm -rfv /var/local/healthcheck # used by previous versions
rm -rfv /run/healthcheck
