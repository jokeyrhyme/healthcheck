#! /usr/bin/env bash

set -eu -o pipefail

mkdir -p /usr/local/bin /run/healthcheck/{ok,error,warning}
chmod a+rx /usr/local/bin /run/healthcheck/{ok,error,warning}
cp -av ./bin/*.sh /usr/local/bin/
cp -av ./systemd/*.{service,timer} /etc/systemd/system/
systemctl daemon-reload
for TIMER in ./systemd/*.timer; do
  if [ "${TIMER}" = "./systemd/healthcheck-report.timer" ]; then
    # the report timer is intended for headless systems,
    # so require manual installation for now
    continue
  fi
  systemctl enable --now "$(basename "${TIMER}")"
done
