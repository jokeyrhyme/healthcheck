#! /usr/bin/env sh

set -eu

if command -v bash >/dev/null; then bash -n bin/*.sh scripts/*.sh; fi
if command -v zsh >/dev/null; then zsh -n bin/*.sh scripts/*.sh; fi
if command -v shellcheck >/dev/null; then shellcheck bin/*.sh scripts/*.sh; fi
if command -v prettier >/dev/null; then prettier --check .; fi
if command -v write-good >/dev/null; then write-good ./*.md; fi
if command -v docker >/dev/null; then docker run -v "${PWD}:/code" bats/bats:latest tests; fi

if command -v systemd-analyze >/dev/null; then
  for UNIT_FILE in ./systemd/*.service ./systemd/*.timer; do
    systemd-analyze verify "${UNIT_FILE}"
  done
fi
