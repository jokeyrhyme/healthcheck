#! /usr/bin/env sh

set -eu

if command -v shfmt >/dev/null; then shfmt -w -s -i 2 bin/*.sh scripts/*.sh; fi
if command -v prettier >/dev/null; then prettier --write .; fi
